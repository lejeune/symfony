(function($) {
    $("#month").datepicker({
        autoclose: true,
        autoHide: true,
        minViewMode: 1,
        format: 'mm/yyyy'
    }).on('pick.datepicker', function(selected){
            var $month = $("#month").datepicker('getDate', true);
            loadTransactionsData($month);
    });

    function loadTransactionsData($month){
        var jqxhr = $.get( "/ajax/transactions", { month: $month })
            .done(function(data) {
                if (typeof data.status != "undefined" && data.status != "undefined")
                    {
                        if (data.status == "OK")
                        {
                            if (typeof data.transactions != "undefined" && data.transactions != "undefined")
                            {
                                //Clear all data first
                                $(".transactions-table > tbody").html("");

                                if(data.transactions.length !== 0){
                                    $.each(data.transactions, function( index, transaction ) {
                                        $(".transactions-table").find('tbody')
                                        .append($('<tr>')
                                            .append($('<td>')
                                                .append(transaction.id)
                                            )
                                            .append($('<td>')
                                                .append(transaction.title)
                                            )
                                            .append($('<td>')
                                                .append(transaction.description)
                                            )
                                            .append($('<td>')
                                                .append(transaction.amount)
                                            )
                                            .append($('<td>')
                                                .append(transaction.is_input)
                                            )
                                            .append($('<td>')
                                                .append(transaction.is_valid)
                                            )
                                            .append($('<td>')
                                                .append(transaction.month)
                                            )
                                        );
                                      });
                                }else{
                                    $(".transactions-table").find('tbody')
                                        .append($('<tr>')
                                            .append($('<td colspan="7">')
                                                .append('Pas de transaction pour le mois séléctionné')
                                            )
                                        );
                                }
                            }
                            if (typeof data.input != "undefined" && data.input != "undefined")
                            {
                                $(".input").html(data.input);
                            }
                            if (typeof data.output != "undefined" && data.output != "undefined")
                            {
                                $(".output").html(data.output);
                            }
                            if (typeof data.amountBeforeMonth != "undefined" && data.amountBeforeMonth != "undefined")
                            {
                                $(".amountBeforeMonth").html(data.amountBeforeMonth);
                            }
                            if (typeof data.amountIncludingMonth != "undefined" && data.amountIncludingMonth != "undefined")
                            {
                                $(".amountIncludingMonth").html(data.amountIncludingMonth);
                            }
                        }
                    }
            })
            .fail(function() {});
    }

    //By default load current month
    var $month = $("#month").datepicker('getDate', true);
    loadTransactionsData($month);
})(jQuery);