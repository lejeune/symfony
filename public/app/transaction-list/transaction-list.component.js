angular.
  module('transactionList').
  component('transactionList', {
    templateUrl: 'transaction-list/transaction-list.template.html',
    controller: function TransactionListController($http) {
        var self = this;
        this.default_month = new Date();

        this.filterByMonth = function () {
            var selected_month = (self.default_month.getMonth() + 1) + '/' + self.default_month.getFullYear();
            var req = {
                method: 'GET',
                url: '/ajax/transactions?month=' + selected_month,
                headers: {
                    'Content-Type': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest'
                }
               }

               $http(req).then(function(response){
                    self.transactions = response.data.transactions;

                    self.input = response.data.input;
                    self.output = response.data.output;
                    self.amountBeforeMonth = response.data.amountBeforeMonth;
                    self.amountIncludingMonth = response.data.amountIncludingMonth;
               });
        }

        this.filterByMonth();
    }
  });