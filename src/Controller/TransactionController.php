<?php

namespace App\Controller;

use App\Entity\Transaction;
use App\Form\TransactionType;
use App\Repository\TransactionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class TransactionController extends Controller
{
    /**
     * @Route("/admin/transactions", name="transaction_index", methods={"GET"})
     */
    public function index(Request $request, TransactionRepository $transactionRepository): Response
    {

        $title_filter = '';

        if ($request->query->getAlnum('filter')) {
            $title_filter = $request->query->getAlnum('filter');
        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $transactionRepository->getPaginationQuery($title_filter),
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('transaction/admin/index.html.twig', [
            'transactions' => $pagination,
        ]);
    }

    /**
     * @Route("/admin/transactions/new", name="transaction_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $transaction = new Transaction();
        $form = $this->createForm(TransactionType::class, $transaction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($transaction);
            $entityManager->flush();

            return $this->redirectToRoute('transaction_index');
        }

        return $this->render('transaction/admin/new.html.twig', [
            'transaction' => $transaction,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="transaction_show", methods={"GET"})
     */
    public function show(Transaction $transaction): Response
    {
        return $this->render('transaction/admin/show.html.twig', [
            'transaction' => $transaction,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="transaction_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Transaction $transaction): Response
    {
        $form = $this->createForm(TransactionType::class, $transaction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $transaction->setUpdatedAt(new \DateTime());

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('transaction_index', [
                'id' => $transaction->getId(),
            ]);
        }

        return $this->render('transaction/admin/edit.html.twig', [
            'transaction' => $transaction,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="transaction_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Transaction $transaction): Response
    {
        if ($this->isCsrfTokenValid('delete'.$transaction->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($transaction);
            $entityManager->flush();
        }

        return $this->redirectToRoute('transaction_index');
    }

    /**
     * @Route("/", name="transaction_list", methods={"GET"})
     */
    public function home(Request $request, TransactionRepository $transactionRepository): Response
    {
        return $this->render('transaction/index.html.twig', [
            'currentMonth' => date('m/Y', time()),
        ]);
    }

    /**
     * @Route("/ajax/transactions", name="transaction_ajax_list", methods={"GET"})
     */
    public function ajaxList(Request $request, TransactionRepository $transactionRepository): Response
    {
        if (!$request->isXmlHttpRequest() || !$request->query->get('month')) {
            return new JsonResponse(array(
                'status' => 'Error',
                'message' => 'Error'),
            400);
        }

        $month = $request->query->get('month');

        $transactions           = $transactionRepository->getByMonth($month);
        $amountBeforeMonth      = $transactionRepository->getAmountBeforeMonth($month);
        $transactions_formatted = array();

        $output = 0;
        $input  = 0;
        $amountIncludingMonth  = 0;

        foreach($transactions as $transaction){

            if($transaction->getIsInput()){
                $input  += $transaction->getAmount();
            }else{
                $output += $transaction->getAmount();
            }

            $amountIncludingMonth += $transaction->getAmount();

            array_push($transactions_formatted, array(
                'id'    => '#' . $transaction->getId(),
                'title' => $transaction->getTitle(),
                'description' => $transaction->getDescription(),
                'amount' => $transaction->getAmount() . '€',
                'is_input' => $transaction->getIsInput() ? 'Entrée' : 'Sortie',
                'is_valid' => $transaction->getIsValid() ? 'Oui' : 'Non',
                'month' => $transaction->getCreatedAt()->format('m/Y')
            ));
        }

        if($amountBeforeMonth)
            $amountIncludingMonth += $amountBeforeMonth;

        // Send all this back to client
        return new JsonResponse(array(
            'status'        => 'OK',
            'transactions'  => $transactions_formatted,
            'input'  => number_format($input, 2) . '€',
            'output'  => number_format($output, 2) . '€',
            'amountBeforeMonth'  => $amountBeforeMonth ? number_format($amountBeforeMonth, 2) . '€' : '0.00€',
            'amountIncludingMonth'  => $amountIncludingMonth ? number_format($amountIncludingMonth, 2) . '€' : '0.00€',
        ),
        200);
    }
    
}
