<?php

namespace App\Form;

use App\Entity\Transaction;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class TransactionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, ['label' => 'Titre'])
            ->add('amount', MoneyType::class)
            ->add('is_input', null, ['label' => 'Entrée'])
            ->add('description', null, ['label' => 'Description'])
            ->add('is_valid', null, ['label' => 'Valide?'])
            //->add('created_at')
            //->add('updated_at')
            ->add('category_id', null, ['label' => 'Catégories'])
            ->add('tags', null, ['label' => 'Tags'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Transaction::class,
        ]);
    }
}
