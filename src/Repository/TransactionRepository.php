<?php

namespace App\Repository;

use App\Entity\Transaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Transaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transaction[]    findAll()
 * @method Transaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Transaction::class);
    }

    // /**
    //  * @return Transaction[] Returns an array of Transaction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Transaction
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getPaginationQuery($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.title LIKE :val')
            ->setParameter('val', '%' . $value . '%')
            ->orderBy('t.created_at', 'ASC')
            ->getQuery()
            ;
    }

    public function getByMonth($month)
    {
        //Format month for database query
        $from   = \DateTimeImmutable::createFromFormat('j/m/Y', '01/' . $month);
        $to     = $from->modify('last day of this month');

        return  $this->createQueryBuilder("t")
            ->andWhere('t.created_at BETWEEN :from AND :to')
            ->andWhere('t.is_valid = :valid')
            ->setParameter('from', $from )
            ->setParameter('to', $to)
            ->setParameter('valid', true)
            ->getQuery()
            ->getResult();
    }

    public function getAmountBeforeMonth($month)
    {
        //Format month for database query
        $before   = \DateTimeImmutable::createFromFormat('j/m/Y', '01/' . $month);

        return  $this->createQueryBuilder("t")
            ->andWhere('t.created_at < :before')
            ->andWhere('t.is_valid = :valid')
            ->setParameter('before', $before )
            ->setParameter('valid', true)
            ->select('SUM(t.amount) as totalAmount')
            ->getQuery()
            ->getSingleScalarResult();
    }
}